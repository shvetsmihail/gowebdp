package main

import (
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"text/template"
)

func main() {
	var wg sync.WaitGroup
	wg.Add(1)

	var err error
	go func() {
		err = OpenDB(wg)
	}()

	if err != nil {
		fmt.Println(err.Error())
	}

	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets/"))))
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/register", registerHandler)
	http.HandleFunc("/edit", editHandler)
	http.HandleFunc("/save", saveHandler)
	http.HandleFunc("/delete", deleteHandler)

	fmt.Println("Listening port: 8080")
	http.ListenAndServe("localhost:8080", nil)
	wg.Done()
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("templates/index.html", "templates/header.html", "templates/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}
	users, err := GetAll()
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	t.ExecuteTemplate(w, "index", users)
}

func registerHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("templates/user.html", "templates/header.html", "templates/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	t.ExecuteTemplate(w, "user", nil)
}

func editHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("templates/user.html", "templates/header.html", "templates/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	user, err := Get(id)
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	t.ExecuteTemplate(w, "user", user)
}

func saveHandler(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	age, err := strconv.Atoi(r.FormValue("age"))
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}
	//TODO "<no value>" change to ""
	if r.FormValue("id") != "<no value>" {
		id, _ := strconv.Atoi(r.FormValue("id"))
		err = Update(id, name, age)
		if err != nil {
			fmt.Fprintf(w, err.Error())
		}
	} else {
		err = Save(name, age)
		if err != nil {
			fmt.Fprintf(w, err.Error())
		}
	}
	http.Redirect(w, r, "/", 302)
}

func deleteHandler(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}
	err = Delete(id)
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}
	http.Redirect(w, r, "/", 302)
}
