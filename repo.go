package main

import (
	"database/sql"
	"sync"
	"webDB/model"

	_ "github.com/go-sql-driver/mysql"
)

var (
	db   sql.DB
	id   int
	name string
	age  int
)

func OpenDB(wg sync.WaitGroup) error {
	dataBase, err := sql.Open("mysql", "root:admin@tcp(localhost:3306)/go")
	if err != nil {
		return err
	}
	defer db.Close()
	db = *dataBase
	wg.Wait()
	return nil
}

func GetAll() ([]model.User, error) {
	users := make([]model.User, 0)
	rows, err := db.Query("select id, name, age from users")

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&id, &name, &age)
		if err != nil {
			return nil, err
		}
		user := model.NewUser(id, name, age)
		users = append(users, user)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return users, nil
}

func Get(id int) (model.User, error) {
	err := db.QueryRow("select id, name, age from users where id =?", id).Scan(&id, &name, &age)
	if err != nil {
		return model.User{}, err
	}
	return model.NewUser(id, name, age), nil
}

func Save(name string, age int) error {
	stmt, err := db.Prepare("insert into users(name, age) values(?, ?)")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(name, age)
	if err != nil {
		return err
	}
	return nil
}

func Update(id int, name string, age int) error {
	stmt, err := db.Prepare("update users set name=?, age=? where id=?")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(name, age, id)
	if err != nil {
		return err
	}
	return nil
}

func Delete(id int) error {
	stmt, err := db.Prepare("delete from users where id=?")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(id)
	if err != nil {
		return err
	}
	return nil
}
