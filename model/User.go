package model

type User struct {
	Id   int
	Name string
	Age  int
}

func NewUser(id int, name string, age int) User {
	return User{id, name, age}
}
